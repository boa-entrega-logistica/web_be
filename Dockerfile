FROM node:16-slim
WORKDIR /app
COPY package.json ./
RUN npm install -g @angular/cli
RUN npm install -g npm@8.6.0
RUN npm install --force
COPY . .
EXPOSE 4200
CMD npm run docker