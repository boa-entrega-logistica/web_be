# Front-end da aplicação Boa Entrega

Projeto gerado com [Angular CLI](https://github.com/angular/angular-cli)
## Iniciando Server Local

Rode o comando `ng serve` para um server local. Navegue no `http://localhost:4200/`. A aplicação irá carregar automaticamente as mudanças nos arquivos.

### Para Teste use as credenciais abaixo:

#### email: `fred.david@pucminas.br`

#### password: `pucfreddavid`

## Rodando local com Docker

Execute o comando `docker-compose up` na raiz do projeto.
