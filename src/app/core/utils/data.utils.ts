import moment from "moment";

export  function idade(dataNascimento) {
    return moment().diff(dataNascimento, 'years');
}