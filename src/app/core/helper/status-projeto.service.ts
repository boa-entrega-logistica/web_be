import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HelperService } from './helper.service';
import { StatusProjeto } from './model/status-projeto.model';

@Injectable({
  providedIn: 'root'
})
export class StatusProjetoService extends HelperService{

  private static readonly ENDPOINT_RAIZ_STATUS_PROJETO_HELPER = `${StatusProjetoService.ENDPOINT_RAIZ_HELPER}/status-projeto`;

  constructor(private http: HttpClient) {
    super();
  }
  
  getStatusProjeto(): Observable<StatusProjeto[]> {
    return this.http.get<StatusProjeto[]>(StatusProjetoService.ENDPOINT_RAIZ_STATUS_PROJETO_HELPER);
  }
}
