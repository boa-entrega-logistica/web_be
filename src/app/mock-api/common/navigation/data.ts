/* tslint:disable:max-line-length */
import { FuseNavigationItem } from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id: 'principal',
        title: 'Principal',
        type: 'basic',
        icon: 'heroicons_outline:home',
        link: '/principal'
    },
    {
        type: 'divider',
    },
    {
        id: 'cadastros',
        title: 'Cadastros',
        type: 'aside',
        icon: 'heroicons_outline:user-add',
        children: [
            {
                id: 'cliente',
                title: 'Clientes',
                type: 'basic',
                link: '/cadastros/cliente',
                tooltip: "Espaço para Cadastrar, Editar, Listar e Inativar um Cliente"
            },
            {
                id: 'deposito',
                title: 'Depósitos',
                type: 'basic',
                link: '/cadastros/deposito',
                tooltip: "Espaço para Cadastrar, Editar, Listar e Inativar um Depósito"
            },
            {
                id: 'entregador',
                title: 'Entregadores',
                type: 'basic',
                link: '/cadastros/entregador',
                tooltip: "Espaço para Cadastrar, Editar, Listar e Inativar um Entregador"
            }
        ]
    },
    {
        id: 'entregas',
        title: 'Entregas',
        type: 'aside',
        icon: 'heroicons_outline:truck',
        children: [
            {
                id: 'rota',
                title: 'Rotas',
                type: 'basic',
                link: '/entregas/rota',
                tooltip: "Espaço para Cadastrar, Editar, Listar e Inativar um Rotas"
            }
        ]
    }

];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id: 'principal',
        title: 'Principal',
        type: 'basic',
        icon: 'heroicons_outline:home',
        link: '/principal'
    },
    {
        type: 'divider',
    },
    {
        id: 'cadastros',
        title: 'Cadastros',
        type: 'aside',
        icon: 'heroicons_outline:user-add',
        children: [
            {
                id: 'cliente',
                title: 'Clientes',
                type: 'basic',
                link: '/cadastros/cliente',
                tooltip: "Espaço para Cadastrar, Editar, Listar e Inativar um Cliente"
            },
            {
                id: 'deposito',
                title: 'Depósitos',
                type: 'basic',
                link: '/cadastros/deposito',
                tooltip: "Espaço para Cadastrar, Editar, Listar e Inativar um Depósito"
            },
            {
                id: 'entregador',
                title: 'Entregadores',
                type: 'basic',
                link: '/cadastros/entregador',
                tooltip: "Espaço para Cadastrar, Editar, Listar e Inativar um Entregador"
            }
        ]
    },
    {
        id: 'entregas',
        title: 'Entregas',
        type: 'aside',
        icon: 'heroicons_outline:truck',
        children: [
            {
                id: 'rota',
                title: 'Rotas',
                type: 'basic',
                link: '/entregas/rota',
                tooltip: "Espaço para Cadastrar, Editar, Listar e Inativar um Rotas"
            }
        ]
    }
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id: 'principal',
        title: 'Principal',
        type: 'basic',
        icon: 'heroicons_outline:home',
        link: '/principal'
    },
    {
        type: 'divider',
    },
    {
        id: 'cadastros',
        title: 'Cadastros',
        type: 'aside',
        icon: 'heroicons_outline:user-add',
        children: [
            {
                id: 'cliente',
                title: 'Clientes',
                type: 'basic',
                link: '/cadastros/cliente',
                tooltip: "Espaço para Cadastrar, Editar, Listar e Inativar um Cliente"
            },
            {
                id: 'deposito',
                title: 'Depósitos',
                type: 'basic',
                link: '/cadastros/deposito',
                tooltip: "Espaço para Cadastrar, Editar, Listar e Inativar um Depósito"
            },
            {
                id: 'entregador',
                title: 'Entregadores',
                type: 'basic',
                link: '/cadastros/entregador',
                tooltip: "Espaço para Cadastrar, Editar, Listar e Inativar um Entregador"
            }
        ]
    },
    {
        id: 'entregas',
        title: 'Entregas',
        type: 'aside',
        icon: 'heroicons_outline:truck',
        children: [
            {
                id: 'rota',
                title: 'Rotas',
                type: 'basic',
                link: '/entregas/rota',
                tooltip: "Espaço para Cadastrar, Editar, Listar e Inativar um Rotas"
            }
        ]
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id: 'principal',
        title: 'Principal',
        type: 'basic',
        icon: 'heroicons_outline:home',
        link: '/principal'
    },
    {
        type: 'divider',
    },
    {
        id: 'cadastros',
        title: 'Cadastros',
        type: 'aside',
        link: '/cadastros',
        icon: 'heroicons_outline:user-add',
        children: [
            {
                id: 'cliente',
                title: 'Clientes',
                type: 'basic',
                link: '/cadastros/cliente',
                tooltip: "Espaço para Cadastrar, Editar, Listar e Inativar um Cliente"
            },
            {
                id: 'deposito',
                title: 'Depósitos',
                type: 'basic',
                link: '/cadastros/deposito',
                tooltip: "Espaço para Cadastrar, Editar, Listar e Inativar um Depósito"
            },
            {
                id: 'entregador',
                title: 'Entregadores',
                type: 'basic',
                link: '/cadastros/entregador',
                tooltip: "Espaço para Cadastrar, Editar, Listar e Inativar um Entregador"
            }
        ]
    },
    {
        id: 'entregas',
        title: 'Entregas',
        type: 'aside',
        icon: 'heroicons_outline:truck',
        children: [
            {
                id: 'rota',
                title: 'Rotas',
                type: 'basic',
                link: '/entregas/rota',
                tooltip: "Espaço para Cadastrar, Editar, Listar e Inativar um Rotas"
            }
        ]
    }
];
