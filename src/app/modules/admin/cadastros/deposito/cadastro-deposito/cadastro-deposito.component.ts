import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {FuseConfirmationService} from "../../../../../../@fuse/services/confirmation";
import { Deposito } from '../model/deposito.model';
import { DepositoService } from '../deposito.service';

@Component({
    selector: 'cadastros',
    templateUrl: './cadastro-deposito.component.html',
    encapsulation: ViewEncapsulation.None
})
export class CadastroDepositoComponent implements OnInit {

    private deposito = {} as Deposito;
    private _id: bigint = null;
    thisExist = false;
    cnpjmask: string = "00.000.000/0000-00";
    telmask: string = "(00) 00000-0000";

    depositoForm: FormGroup;
    mensagemErro: string = null;
    botaoSalvarDesabilitado: boolean = false;

    constructor(private _formBuilder: FormBuilder,
                private _depositoService: DepositoService,
                private _router: Router,
                private _fuseConfirmationService: FuseConfirmationService,
                private _activatedRoute: ActivatedRoute) {
    }

    ngOnInit(): void {

        this.depositoForm = this._formBuilder.group({
            nome: [''],
            cnpj: [''],
            telefone: [''],
            logradouro: [''],
            numero: [''],
            bairro: [''],
            cep: [''],
            municipio: [''],
            estado: [''],
            latitude: [''],
            longitude: [''],
        });

        this._carregardeposito();
    }


    private _carregardeposito() {
        this._id = this._activatedRoute.snapshot.params.id;
        if (this._id) {
            this.thisExist = true;
            return this._depositoService.buscarDepositoPorId(this._id)
                .subscribe(
                    deposito => {
                        this._carregarInformacoesdeposito(deposito);
                    }
                );
        }
    }

    private _carregarInformacoesdeposito(deposito: Deposito) {
        this.depositoForm.get('nome').setValue(deposito.nome);
        this.depositoForm.get('cnpj').setValue(deposito.cnpj);
        this.depositoForm.get('telefone').setValue(deposito.telefone);
        this.depositoForm.get('logradouro').setValue(deposito.endereco.logradouro);
        this.depositoForm.get('numero').setValue(deposito.endereco.numero);
        this.depositoForm.get('bairro').setValue(deposito.endereco.bairro);
        this.depositoForm.get('cep').setValue(deposito.endereco.cep);
        this.depositoForm.get('municipio').setValue(deposito.endereco.municipio);
        this.depositoForm.get('estado').setValue(deposito.endereco.estado);
        this.depositoForm.get('latitude').setValue(deposito.endereco.latitude);
        this.depositoForm.get('longitude').setValue(deposito.endereco.longitude);
    }

    salvar() {
        this.botaoSalvarDesabilitado = true;
        this.deposito = this._payloadDeposito();

        if (this._id) {
            this._depositoService.atualizar(this._id, this.deposito)
                .subscribe(
                    () => {
                        this._resetarFormulario();
                        this.botaoSalvarDesabilitado = false;
                        this._router.navigateByUrl('/cadastros/deposito');
                    },
                    error => {
                        this.botaoSalvarDesabilitado = false;
                        this.mensagemErro = error.error.message;
                    }
                );
            return;
        }

        this._depositoService.salvar(this.deposito)
            .subscribe(
                () => {
                    this._resetarFormulario();
                    this.botaoSalvarDesabilitado = false;
                    this._router.navigateByUrl('/cadastros/deposito');
                },
                error => {
                    this.botaoSalvarDesabilitado = false;
                    this.mensagemErro = error.error.message;
                }
            );

    }

    private _resetarFormulario() {
        this.depositoForm.reset();
        this.deposito = {} as Deposito;
        this.botaoSalvarDesabilitado = false
    }

    private _payloadDeposito(): Deposito {
        const payload = Object.assign({}, this.depositoForm.value);
        return {
            nome: payload.nome,
            cnpj: payload.cnpj,
            telefone: payload.telefone,
            endereco: {
                logradouro: payload.logradouro,
                numero: payload.numero,
                bairro: payload.bairro,
                cep: payload.cep,
                municipio: payload.municipio,
                estado: payload.estado,
                latitude: payload.latitude,
                longitude: payload.longitude
            }
        }
    }
}
