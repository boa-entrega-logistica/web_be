import {Injectable} from "@angular/core";
import {environment} from "../../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Deposito} from "./model/deposito.model";

@Injectable({
    providedIn: 'root'
})
export class DepositoService {

    private static readonly ENDPOINT_RAIZ_DEPOSITO = `${environment.api_boaentrega_url}/deposito`

    constructor(private http: HttpClient) { }

    salvar(deposito: Deposito): Observable<Deposito>{
        return this.http.post<Deposito>(DepositoService.ENDPOINT_RAIZ_DEPOSITO, deposito);
    }

    atualizar( id: bigint, deposito: Deposito): Observable<Deposito>{
        return this.http.put<Deposito>(`${DepositoService.ENDPOINT_RAIZ_DEPOSITO}/${id}`, deposito);
    }

    deletar(id: bigint): Observable<any>{
        return this.http.delete(`${DepositoService.ENDPOINT_RAIZ_DEPOSITO}/${id}`);
    }

    buscarDepositoPorId(id: bigint): Observable<Deposito>{
        return this.http.get<Deposito>(`${DepositoService.ENDPOINT_RAIZ_DEPOSITO}/${id}`)
    }

    buscarDepositoPorCpfCnpj(cnpj: string): Observable<Deposito>{
        return this.http.get<Deposito>(`${DepositoService.ENDPOINT_RAIZ_DEPOSITO}/${cnpj}`)
    }

    getDepositos(): Observable<Array<Deposito>>{
        return this.http.get<Array<Deposito>>(`${DepositoService.ENDPOINT_RAIZ_DEPOSITO}`)
    }

}