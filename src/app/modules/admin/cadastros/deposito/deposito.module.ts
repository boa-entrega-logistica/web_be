import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import {MatButtonModule} from "@angular/material/button";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {ReactiveFormsModule} from "@angular/forms";
import {NgxMaskModule} from "ngx-mask";
import {MatDividerModule} from "@angular/material/divider";
import {MatCardModule} from "@angular/material/card";
import {FuseAlertModule} from "../../../../../@fuse/components/alert";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../../../../shared/shared.module";
import {MatIconModule} from "@angular/material/icon";
import {MatTableModule} from "@angular/material/table";
import {FuseConfirmationModule} from "../../../../../@fuse/services/confirmation";
import {MatMenuModule} from "@angular/material/menu";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatSortModule} from "@angular/material/sort";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {FuseLoadingBarModule} from "../../../../../@fuse/components/loading-bar";
import {MatChipsModule} from "@angular/material/chips";
import {MatTooltipModule} from "@angular/material/tooltip";
import {CadastroDepositoComponent} from "./cadastro-deposito/cadastro-deposito.component";
import {DepositoComponent} from "./deposito.component";

const depositoRoutes: Route[] = [
    {
        path     : '',
        component: DepositoComponent
    },
    {
        path     : 'cadastro',
        component: CadastroDepositoComponent
    },
    {
        path     : 'cadastro/:id',
        component: CadastroDepositoComponent
    },
    {
        path     : '',
        component: CadastroDepositoComponent
    }
];

@NgModule({
    declarations: [
        DepositoComponent,
        CadastroDepositoComponent
    ],
    imports     : [
        RouterModule.forChild(depositoRoutes),
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        ReactiveFormsModule,
        NgxMaskModule,
        MatDividerModule,
        MatCardModule,
        FuseAlertModule,
        CommonModule,
        SharedModule,
        MatIconModule,
        MatTableModule,
        FuseConfirmationModule,
        MatMenuModule,
        MatCheckboxModule,
        MatSortModule,
        MatAutocompleteModule,
        FuseLoadingBarModule,
        MatChipsModule,
        MatTooltipModule
    ]
})
export class DepositoModule
{
}
