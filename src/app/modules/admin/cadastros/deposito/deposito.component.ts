import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Router} from "@angular/router";
import {FuseAlertType} from "../../../../../@fuse/components/alert";
import {Deposito} from "./model/deposito.model";
import { DepositoService } from './deposito.service';

@Component({
    selector     : 'cadastros',
    templateUrl  : './deposito.component.html',
    encapsulation: ViewEncapsulation.None
})
export class DepositoComponent implements OnInit {
    displayedColumns: string[] = [
        "nome",
        "cnpj",
        "telefone",
        "opcoes"
    ];
    depositos: Array<Deposito> = [];
    dataSource: MatTableDataSource<Deposito>;
    isEmptyList: boolean = false;

    alert: { type: FuseAlertType; message: string } = {
        type: "success",
        message: "",
    };
    showAlert: boolean = false;

    constructor(
        private _depositoService: DepositoService,
        private _router: Router
    ){}

    ngOnInit(): void {
        this.carregarDepositos();
    }

    carregarDepositos() {
        this._depositoService.getDepositos().subscribe(
            (response) => {
                this.depositos = response;
                this.isEmptyList = this.depositos.length == 0;
                this.dataSource = new MatTableDataSource<Deposito>(
                    this.depositos
                );
            },
            (exception) => {
                console.log(exception);
                this.isEmptyList = true;
            }
        )
    }

    atualizarDataTable() {
        this._depositoService.getDepositos().subscribe(
            (response) => {
                this.dataSource.data = response;
                this.isEmptyList = this.dataSource.data.length == 0;
            },
            (exception) => {
                console.log(exception);
                this.isEmptyList = true;
            }
        )
    }

    filtrar(event: Event) {
        const valorFiltro = (event.target as HTMLInputElement).value;
        this.dataSource.filter = valorFiltro.trim().toLowerCase();

    }

    editar(id: string) {
        return `/cadastros/deposito/cadastro/${id}`;
    }

    deletar(deposito: Deposito) {
        this._depositoService.deletar(deposito.id).subscribe(
            () => {
                this.alert = {
                    type: "success",
                    message: `O deposito ${deposito.nome} foi deletado com sucesso.`,
                };
                this.showAlert = true;
                this.atualizarDataTable();
            },
            (exception) => {
                console.log(exception);
            }
        );
    }
}
