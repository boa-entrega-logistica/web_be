import {Endereco} from "../../model/endereco.model";

export interface Deposito {
    id?: bigint;
    nome: string;
    cnpj: string;
    telefone: string;
    endereco: Endereco;
}