export interface Endereco {
    id?: bigint;
    logradouro: string;
    numero: number;
    bairro: string;
    cep: string;
    municipio: string;
    estado: string;
    latitude: string;
    longitude: string;
}