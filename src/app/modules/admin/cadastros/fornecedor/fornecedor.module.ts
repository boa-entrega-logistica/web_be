import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import {FornecedorComponent} from "./fornecedor.component";

const exampleRoutes: Route[] = [
    {
        path     : '',
        component: FornecedorComponent
    }
];

@NgModule({
    declarations: [
        FornecedorComponent
    ],
    imports     : [
        RouterModule.forChild(exampleRoutes)
    ]
})
export class FornecedorModule
{
}
