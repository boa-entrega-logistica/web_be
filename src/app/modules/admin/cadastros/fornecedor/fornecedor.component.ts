import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector     : 'cadastros',
    templateUrl  : './fornecedor.component.html',
    encapsulation: ViewEncapsulation.None
})
export class FornecedorComponent
{
    /**
     * Constructor
     */
    constructor()
    {
    }
}
