import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import {EntregadorComponent} from "./entregador.component";
import {CadastroEntregadorComponent} from "./cadastro-entregador/cadastro-entregador.component";
import {MatButtonModule} from "@angular/material/button";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {ReactiveFormsModule} from "@angular/forms";
import {NgxMaskModule} from "ngx-mask";
import {MatCardModule} from "@angular/material/card";
import {FuseAlertModule} from "../../../../../@fuse/components/alert";
import {CommonModule} from "@angular/common";
import {MatDividerModule} from "@angular/material/divider";
import {SharedModule} from "../../../../shared/shared.module";
import {MatIconModule} from "@angular/material/icon";
import {MatTableModule} from "@angular/material/table";
import {MatMenuModule} from "@angular/material/menu";
import {FuseConfirmationModule} from "../../../../../@fuse/services/confirmation";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatSortModule} from "@angular/material/sort";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {FuseLoadingBarModule} from "../../../../../@fuse/components/loading-bar";
import {MatChipsModule} from "@angular/material/chips";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatRadioModule} from "@angular/material/radio";

const entregadorRoutes: Route[] = [
    {
        path     : '',
        component: EntregadorComponent
    },
    {
        path     : 'cadastro',
        component: CadastroEntregadorComponent
    },
    {
        path     : 'cadastro/:id',
        component: CadastroEntregadorComponent
    }
];

@NgModule({
    declarations: [
        EntregadorComponent,
        CadastroEntregadorComponent
    ],
    imports: [
        RouterModule.forChild(entregadorRoutes),
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        ReactiveFormsModule,
        NgxMaskModule,
        MatDividerModule,
        MatCardModule,
        FuseAlertModule,
        CommonModule,
        SharedModule,
        MatIconModule,
        MatTableModule,
        FuseConfirmationModule,
        MatMenuModule,
        MatCheckboxModule,
        MatSortModule,
        MatAutocompleteModule,
        FuseLoadingBarModule,
        MatChipsModule,
        MatTooltipModule,
        MatRadioModule
    ]
})
export class EntregadorModule
{
}
