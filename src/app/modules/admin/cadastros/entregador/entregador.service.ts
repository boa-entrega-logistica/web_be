import {Injectable} from "@angular/core";
import {environment} from "../../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Entregador} from "./model/entregador.model";

@Injectable({
    providedIn: 'root'
})
export class EntregadorService {

    private static readonly ENDPOINT_RAIZ_ENTREGADOR = `${environment.api_boaentrega_url}/entregador`

    constructor(private http: HttpClient) { }

    salvar(entregador: Entregador): Observable<Entregador>{
        return this.http.post<Entregador>(EntregadorService.ENDPOINT_RAIZ_ENTREGADOR, entregador);
    }

    atualizar( id: bigint, entregador: Entregador): Observable<Entregador>{
        return this.http.put<Entregador>(`${EntregadorService.ENDPOINT_RAIZ_ENTREGADOR}/${id}`, entregador);
    }

    deletar(id: bigint): Observable<any>{
        return this.http.delete(`${EntregadorService.ENDPOINT_RAIZ_ENTREGADOR}/${id}`);
    }

    buscarEntregadorPorId(id: bigint): Observable<Entregador>{
        return this.http.get<Entregador>(`${EntregadorService.ENDPOINT_RAIZ_ENTREGADOR}/${id}`)
    }

    buscarEntregadorPorCpfCnpj(cpfCnpj: string): Observable<Entregador>{
        return this.http.get<Entregador>(`${EntregadorService.ENDPOINT_RAIZ_ENTREGADOR}/${cpfCnpj}`)
    }

    getEntregadores(): Observable<Array<Entregador>>{
        return this.http.get<Array<Entregador>>(`${EntregadorService.ENDPOINT_RAIZ_ENTREGADOR}`)
    }

}