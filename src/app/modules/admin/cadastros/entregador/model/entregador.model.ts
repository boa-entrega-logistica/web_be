export interface Entregador {
    id?: bigint;
    latitudeAgora: string;
    longitudeAgora: string;
    nome: string;
    telefone: string;
    placaVeiculo: string;
    cpf: string;
}