import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Entregador} from "../model/entregador.model";
import {FormBuilder, FormGroup} from "@angular/forms";
import {EntregadorService} from '../entregador.service';
import {ActivatedRoute, Router} from "@angular/router";
import {FuseConfirmationService} from "../../../../../../@fuse/services/confirmation";

@Component({
    selector: 'cadastros',
    templateUrl: './cadastro-entregador.component.html',
    encapsulation: ViewEncapsulation.None
})
export class CadastroEntregadorComponent implements OnInit {

    private entregador = {} as Entregador;
    private _id: bigint = null;
    thisExist = false;
    cpfmask: string = "000.000.000-00";
    telmask: string = "(00) 00000-0000";
    placamask: string = "AAA-0000||AAA0A00";

    entregadorForm: FormGroup;
    mensagemErro: string = null;
    botaoSalvarDesabilitado: boolean = false;

    constructor(private _formBuilder: FormBuilder,
                private _entregadorService: EntregadorService,
                private _router: Router,
                private _fuseConfirmationService: FuseConfirmationService,
                private _activatedRoute: ActivatedRoute) {
    }

    ngOnInit(): void {

        this.entregadorForm = this._formBuilder.group({
            nome: [''],
            cpf: [''],
            telefone: [''],
            latitudeAgora: [''],
            longitudeAgora: [''],
            placaVeiculo: [''],
        });

        this._carregarEntregador();
    }


    private _carregarEntregador() {
        this._id = this._activatedRoute.snapshot.params.id;
        if (this._id) {
            this.thisExist = true;
            return this._entregadorService.buscarEntregadorPorId(this._id)
                .subscribe(
                    entregador => {
                        this._carregarInformacoesEntregador(entregador);
                    }
                );
        }
    }

    private _carregarInformacoesEntregador(entregador: Entregador) {
        this.entregadorForm.get('nome').setValue(entregador.nome);
        this.entregadorForm.get('cpf').setValue(entregador.cpf);
        this.entregadorForm.get('telefone').setValue(entregador.telefone);
        this.entregadorForm.get('placaVeiculo').setValue(entregador.placaVeiculo);
        this.entregadorForm.get('latitudeAgora').setValue(entregador.latitudeAgora);
        this.entregadorForm.get('longitudeAgora').setValue(entregador.longitudeAgora);
    }

    salvar() {
        this.botaoSalvarDesabilitado = true;
        this.entregador = this._payloadEntregador();

        if (this._id) {
            this._entregadorService.atualizar(this._id, this.entregador)
                .subscribe(
                    () => {
                        this._resetarFormulario();
                        this.botaoSalvarDesabilitado = false;
                        this._router.navigateByUrl('/cadastros/entregador');
                    },
                    error => {
                        this.botaoSalvarDesabilitado = false;
                        this.mensagemErro = error.error.message;
                    }
                );
            return;
        }

        this._entregadorService.salvar(this.entregador)
            .subscribe(
                () => {
                    this._resetarFormulario();
                    this.botaoSalvarDesabilitado = false;
                    this._router.navigateByUrl('/cadastros/entregador');
                },
                error => {
                    this.botaoSalvarDesabilitado = false;
                    this.mensagemErro = error.error.message;
                }
            );

    }

    private _resetarFormulario() {
        this.entregadorForm.reset();
        this.entregador = {} as Entregador;
        this.botaoSalvarDesabilitado = false
    }

    private _payloadEntregador(): Entregador {
        const payload = Object.assign({}, this.entregadorForm.value);
        return {
            latitudeAgora: payload.latitudeAgora,
            longitudeAgora: payload.longitudeAgora,
            nome: payload.nome,
            telefone: payload.telefone,
            placaVeiculo: payload.placaVeiculo,
            cpf: payload.cpf,
        }
    }
}
