import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Entregador} from "./model/entregador.model";
import {EntregadorService} from "./entregador.service";
import {Router} from "@angular/router";
import {FuseAlertType} from "../../../../../@fuse/components/alert";

@Component({
    selector     : 'cadastros',
    templateUrl  : './entregador.component.html',
    encapsulation: ViewEncapsulation.None
})
export class EntregadorComponent implements OnInit {
    displayedColumns: string[] = [
        "nome",
        "cpf",
        "placaVeiculo",
        "telefone",
        "opcoes"
    ];
    entregadors: Array<Entregador> = [];
    dataSource: MatTableDataSource<Entregador>;
    isEmptyList: boolean = false;

    alert: { type: FuseAlertType; message: string } = {
        type: "success",
        message: "",
    };
    showAlert: boolean = false;

    constructor(
        private _entregadorService: EntregadorService,
        private _router: Router
    ){}

    ngOnInit(): void {
        this.carregarEntregadores();
    }

    carregarEntregadores() {
        this._entregadorService.getEntregadores().subscribe(
            (response) => {
                this.entregadors = response;
                this.isEmptyList = this.entregadors.length == 0;
                this.dataSource = new MatTableDataSource<Entregador>(
                    this.entregadors
                );
            },
            (exception) => {
                console.log(exception);
                this.isEmptyList = true;
            }
        )
    }

    atualizarDataTable() {
        this._entregadorService.getEntregadores().subscribe(
            (response) => {
                this.dataSource.data = response;
                this.isEmptyList = this.dataSource.data.length == 0;
            },
            (exception) => {
                console.log(exception);
                this.isEmptyList = true;
            }
        )
    }

    filtrar(event: Event) {
        const valorFiltro = (event.target as HTMLInputElement).value;
        this.dataSource.filter = valorFiltro.trim().toLowerCase();

    }

    editar(id: string) {
        return `/cadastros/entregador/cadastro/${id}`;
    }

    deletar(entregador: Entregador) {
        this._entregadorService.deletar(entregador.id).subscribe(
            () => {
                this.alert = {
                    type: "success",
                    message: `O entregador ${entregador.nome} foi deletado com sucesso.`,
                };
                this.showAlert = true;
                this.atualizarDataTable();
            },
            (exception) => {
                console.log(exception);
            }
        );
    }
}
