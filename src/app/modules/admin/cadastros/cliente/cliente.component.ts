import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Cliente} from "./model/cliente.model";
import {ClienteService} from "./cliente.service";
import {Router} from "@angular/router";
import {FuseAlertType} from "../../../../../@fuse/components/alert";

@Component({
    selector     : 'cadastros',
    templateUrl  : './cliente.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ClienteComponent implements OnInit {
    displayedColumns: string[] = [
        "nome",
        "cpfCnpj",
        "telefone",
        "opcoes"
    ];
    clientes: Array<Cliente> = [];
    dataSource: MatTableDataSource<Cliente>;
    isEmptyList: boolean = false;

    alert: { type: FuseAlertType; message: string } = {
        type: "success",
        message: "",
    };
    showAlert: boolean = false;

    constructor(
        private _clienteService: ClienteService,
        private _router: Router
    ){}

    ngOnInit(): void {
        this.carregarClientes();
    }

    carregarClientes() {
        this._clienteService.getClientes().subscribe(
            (response) => {
                this.clientes = response;
                this.isEmptyList = this.clientes.length == 0;
                this.dataSource = new MatTableDataSource<Cliente>(
                    this.clientes
                );
            },
            (exception) => {
                console.log(exception);
                this.isEmptyList = true;
            }
        )
    }

    atualizarDataTable() {
        this._clienteService.getClientes().subscribe(
            (response) => {
                this.dataSource.data = response;
                this.isEmptyList = this.dataSource.data.length == 0;
            },
            (exception) => {
                console.log(exception);
                this.isEmptyList = true;
            }
        )
    }

    filtrar(event: Event) {
        const valorFiltro = (event.target as HTMLInputElement).value;
        this.dataSource.filter = valorFiltro.trim().toLowerCase();

    }

    editar(id: string) {
        return `/cadastros/cliente/cadastro/${id}`;
    }

    deletar(cliente: Cliente) {
        this._clienteService.deletar(cliente.id).subscribe(
            () => {
                this.alert = {
                    type: "success",
                    message: `O cliente ${cliente.nome} foi deletado com sucesso.`,
                };
                this.showAlert = true;
                this.atualizarDataTable();
            },
            (exception) => {
                console.log(exception);
            }
        );
    }
}
