import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Cliente} from "../model/cliente.model";
import {FormBuilder, FormGroup} from "@angular/forms";
import {ClienteService} from '../cliente.service';
import {ActivatedRoute, Router} from "@angular/router";
import {FuseConfirmationService} from "../../../../../../@fuse/services/confirmation";

@Component({
    selector: 'cadastros',
    templateUrl: './cadastro-cliente.component.html',
    encapsulation: ViewEncapsulation.None
})
export class CadastroClienteComponent implements OnInit {

    private cliente = {} as Cliente;
    private _id: bigint = null;
    thisExist = false;
    cpfcnpjmask: string = "000.000.000-00||00.000.000/0000-00";
    telmask: string = "(00) 00000-0000";

    clienteForm: FormGroup;
    mensagemErro: string = null;
    botaoSalvarDesabilitado: boolean = false;

    constructor(private _formBuilder: FormBuilder,
                private _clienteService: ClienteService,
                private _router: Router,
                private _fuseConfirmationService: FuseConfirmationService,
                private _activatedRoute: ActivatedRoute) {
    }

    ngOnInit(): void {

        this.clienteForm = this._formBuilder.group({
            nome: [''],
            cpfCnpj: [''],
            telefone: [''],
            logradouro: [''],
            numero: [''],
            bairro: [''],
            cep: [''],
            municipio: [''],
            estado: [''],
            latitude: [''],
            longitude: [''],
        });

        this._carregarCliente();
    }


    private _carregarCliente() {
        this._id = this._activatedRoute.snapshot.params.id;
        if (this._id) {
            this.thisExist = true;
            return this._clienteService.buscarClientePorId(this._id)
                .subscribe(
                    cliente => {
                        this._carregarInformacoesCliente(cliente);
                    }
                );
        }
    }

    private _carregarInformacoesCliente(cliente: Cliente) {
        this.clienteForm.get('nome').setValue(cliente.nome);
        this.clienteForm.get('cpfCnpj').setValue(cliente.cpfCnpj);
        this.clienteForm.get('telefone').setValue(cliente.telefone);
        this.clienteForm.get('logradouro').setValue(cliente.endereco.logradouro);
        this.clienteForm.get('numero').setValue(cliente.endereco.numero);
        this.clienteForm.get('bairro').setValue(cliente.endereco.bairro);
        this.clienteForm.get('cep').setValue(cliente.endereco.cep);
        this.clienteForm.get('municipio').setValue(cliente.endereco.municipio);
        this.clienteForm.get('estado').setValue(cliente.endereco.estado);
        this.clienteForm.get('latitude').setValue(cliente.endereco.latitude);
        this.clienteForm.get('longitude').setValue(cliente.endereco.longitude);
    }

    salvar() {
        this.botaoSalvarDesabilitado = true;
        this.cliente = this._payloadCliente();

        if (this._id) {
            this._clienteService.atualizar(this._id, this.cliente)
                .subscribe(
                    () => {
                        this._resetarFormulario();
                        this.botaoSalvarDesabilitado = false;
                        this._router.navigateByUrl('/cadastros/cliente');
                    },
                    error => {
                        this.botaoSalvarDesabilitado = false;
                        this.mensagemErro = error.error.message;
                    }
                );
            return;
        }

        this._clienteService.salvar(this.cliente)
            .subscribe(
                () => {
                    this._resetarFormulario();
                    this.botaoSalvarDesabilitado = false;
                    this._router.navigateByUrl('/cadastros/cliente');
                },
                error => {
                    this.botaoSalvarDesabilitado = false;
                    this.mensagemErro = error.error.message;
                }
            );

    }

    private _resetarFormulario() {
        this.clienteForm.reset();
        this.cliente = {} as Cliente;
        this.botaoSalvarDesabilitado = false
    }

    private _payloadCliente(): Cliente {
        const payload = Object.assign({}, this.clienteForm.value);
        return {
            nome: payload.nome,
            cpfCnpj: payload.cpfCnpj,
            telefone: payload.telefone,
            endereco: {
                logradouro: payload.logradouro,
                numero: payload.numero,
                bairro: payload.bairro,
                cep: payload.cep,
                municipio: payload.municipio,
                estado: payload.estado,
                latitude: payload.latitude,
                longitude: payload.longitude
            }
        }
    }
}
