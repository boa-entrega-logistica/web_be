import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import {ClienteComponent} from "./cliente.component";
import {CadastroClienteComponent} from "./cadastro-cliente/cadastro-cliente.component";
import {MatButtonModule} from "@angular/material/button";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {ReactiveFormsModule} from "@angular/forms";
import {NgxMaskModule} from "ngx-mask";
import {MatCardModule} from "@angular/material/card";
import {FuseAlertModule} from "../../../../../@fuse/components/alert";
import {CommonModule} from "@angular/common";
import {MatDividerModule} from "@angular/material/divider";
import {SharedModule} from "../../../../shared/shared.module";
import {MatIconModule} from "@angular/material/icon";
import {MatTableModule} from "@angular/material/table";
import {MatMenuModule} from "@angular/material/menu";
import {FuseConfirmationModule} from "../../../../../@fuse/services/confirmation";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatSortModule} from "@angular/material/sort";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {FuseLoadingBarModule} from "../../../../../@fuse/components/loading-bar";
import {MatChipsModule} from "@angular/material/chips";
import {MatTooltipModule} from "@angular/material/tooltip";

const clienteRoutes: Route[] = [
    {
        path     : '',
        component: ClienteComponent
    },
    {
        path     : 'cadastro',
        component: CadastroClienteComponent
    },
    {
        path     : 'cadastro/:id',
        component: CadastroClienteComponent
    }
];

@NgModule({
    declarations: [
        ClienteComponent,
        CadastroClienteComponent
    ],
    imports     : [
        RouterModule.forChild(clienteRoutes),
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        ReactiveFormsModule,
        NgxMaskModule,
        MatDividerModule,
        MatCardModule,
        FuseAlertModule,
        CommonModule,
        SharedModule,
        MatIconModule,
        MatTableModule,
        FuseConfirmationModule,
        MatMenuModule,
        MatCheckboxModule,
        MatSortModule,
        MatAutocompleteModule,
        FuseLoadingBarModule,
        MatChipsModule,
        MatTooltipModule
    ]
})
export class ClienteModule
{
}
