import {Injectable} from "@angular/core";
import {environment} from "../../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Cliente} from "./model/cliente.model";

@Injectable({
    providedIn: 'root'
})
export class ClienteService {

    private static readonly ENDPOINT_RAIZ_CLIENTE = `${environment.api_boaentrega_url}/cliente`

    constructor(private http: HttpClient) { }

    salvar(cliente: Cliente): Observable<Cliente>{
        return this.http.post<Cliente>(ClienteService.ENDPOINT_RAIZ_CLIENTE, cliente);
    }

    atualizar( id: bigint, cliente: Cliente): Observable<Cliente>{
        return this.http.put<Cliente>(`${ClienteService.ENDPOINT_RAIZ_CLIENTE}/${id}`, cliente);
    }

    deletar(id: bigint): Observable<any>{
        return this.http.delete(`${ClienteService.ENDPOINT_RAIZ_CLIENTE}/${id}`);
    }

    buscarClientePorId(id: bigint): Observable<Cliente>{
        return this.http.get<Cliente>(`${ClienteService.ENDPOINT_RAIZ_CLIENTE}/${id}`)
    }

    buscarClientePorCpfCnpj(cpfCnpj: string): Observable<Cliente>{
        return this.http.get<Cliente>(`${ClienteService.ENDPOINT_RAIZ_CLIENTE}/${cpfCnpj}`)
    }

    getClientes(): Observable<Array<Cliente>>{
        return this.http.get<Array<Cliente>>(`${ClienteService.ENDPOINT_RAIZ_CLIENTE}`)
    }

}