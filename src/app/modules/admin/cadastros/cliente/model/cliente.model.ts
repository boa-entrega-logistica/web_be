import {Endereco} from "../../model/endereco.model";

export interface Cliente {
    id?: bigint;
    nome: string;
    cpfCnpj: string;
    telefone: string;
    endereco: Endereco;
}