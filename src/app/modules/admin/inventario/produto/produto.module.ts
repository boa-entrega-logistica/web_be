import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import {ProdutoComponent} from "./produto.component";

const exampleRoutes: Route[] = [
    {
        path     : '',
        component: ProdutoComponent
    }
];

@NgModule({
    declarations: [
        ProdutoComponent
    ],
    imports     : [
        RouterModule.forChild(exampleRoutes)
    ]
})
export class ProdutoModule
{
}
