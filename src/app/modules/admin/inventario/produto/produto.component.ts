import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector     : 'cadastros',
    templateUrl  : './produto.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ProdutoComponent
{
    /**
     * Constructor
     */
    constructor()
    {
    }
}
