import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Rota} from "../model/rota.model";
import {FormBuilder, FormGroup} from "@angular/forms";
import {RotaService} from '../rota.service';
import {ActivatedRoute, Router} from "@angular/router";
import {FuseConfirmationService} from "../../../../../../@fuse/services/confirmation";
import {DepositoService} from "../../../cadastros/deposito/deposito.service";
import {ClienteService} from "../../../cadastros/cliente/cliente.service";
import {EntregadorService} from "../../../cadastros/entregador/entregador.service";
import {Cliente} from "../../../cadastros/cliente/model/cliente.model";
import {Entregador} from "../../../cadastros/entregador/model/entregador.model";
import {Deposito} from "../../../cadastros/deposito/model/deposito.model";

@Component({
    selector: 'entregas',
    templateUrl: './cadastro-rota.component.html',
    encapsulation: ViewEncapsulation.None
})
export class CadastroRotaComponent implements OnInit {

    private rota = {} as Rota;
    private deposito: Deposito;
    private cliente: Cliente;
    private _id: bigint = null;
    thisExist = false;

    clientes = Array<Cliente>();
    depositos = Array<Deposito>();
    entregadores = Array<Entregador>();

    rotaForm: FormGroup;
    mensagemErro: string = null;
    botaoSalvarDesabilitado: boolean = false;

    constructor(private _formBuilder: FormBuilder,
                private _rotaService: RotaService,
                private _depositoService: DepositoService,
                private _clienteService: ClienteService,
                private _entregadorService: EntregadorService,
                private _router: Router,
                private _fuseConfirmationService: FuseConfirmationService,
                private _activatedRoute: ActivatedRoute) {
    }

    ngOnInit(): void {

        this.rotaForm = this._formBuilder.group({
            deposito: [''],
            cliente: [''],
            entregador: [''],
        });

        this._carregarRota();
        this._carregarDepositos();
        this._carregarClientes();
        this._carregarEntregadores();
    }


    private _carregarRota() {
        this._id = this._activatedRoute.snapshot.params.id;
        if (this._id) {
            this.thisExist = true;
            return this._rotaService.buscarRotaPorId(this._id)
                .subscribe(
                    rota => {
                        this._carregarInformacoesRota(rota);
                    }
                );
        }
    }

    private _carregarDepositos() {
        this._depositoService.getDepositos()
            .subscribe(
                depositos => {
                    this.depositos = depositos;
                }
            );
    }

    private _carregarClientes() {
        this._clienteService.getClientes()
            .subscribe(
                clientes => {
                    this.clientes = clientes;
                }
            );
    }

    private _carregarEntregadores() {
        this._entregadorService.getEntregadores()
            .subscribe(
                entregadores => {
                    this.entregadores = entregadores;
                }
            );
    }

    private _carregarInformacoesRota(rota: Rota) {
        this.rotaForm.get('deposito').setValue(rota.deposito.id);
        this.rotaForm.get('cliente').setValue(rota.cliente.id);
        this.rotaForm.get('entregador').setValue(rota.entregador.id);
    }

    salvar() {
        this.botaoSalvarDesabilitado = true;
        this.rota = this._payloadRota();

        if (this._id) {
            this._rotaService.atualizar(this._id, this.rota.entregador.id, this.rota.deposito.id, this.rota.cliente.id)
                .subscribe(
                    () => {
                        this._resetarFormulario();
                        this.botaoSalvarDesabilitado = false;
                        this._router.navigateByUrl('/entregas/rota');
                    },
                    error => {
                        this.botaoSalvarDesabilitado = false;
                        this.mensagemErro = error.error.message;
                    }
                );
            return;
        }

        this._rotaService.salvar(this.rota.entregador.id, this.rota.deposito.id, this.rota.cliente.id)
            .subscribe(
                () => {
                    this._resetarFormulario();
                    this.botaoSalvarDesabilitado = false;
                    this._router.navigateByUrl('/entregas/rota');
                },
                error => {
                    this.botaoSalvarDesabilitado = false;
                    this.mensagemErro = error.error.message;
                }
            );

    }

    private _resetarFormulario() {
        this.rotaForm.reset();
        this.rota = {} as Rota;
        this.botaoSalvarDesabilitado = false
    }

    private _payloadRota(): Rota{
        const payload = Object.assign({}, this.rotaForm.value);
        return {
            deposito: this.depositos.filter( deposito => deposito.id === payload.deposito)[0],
            cliente: this.clientes.filter(cliente => cliente.id === payload.cliente)[0],
            entregador: this.entregadores.filter( entregador => entregador.id ===payload.entregador)[0]
        }
    }

}
