import {Injectable} from "@angular/core";
import {environment} from "../../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Rota} from "./model/rota.model";

@Injectable({
    providedIn: 'root'
})
export class RotaService {

    private static readonly ENDPOINT_RAIZ_ROTA = `${environment.api_boaentrega_url}/rota`

    constructor(private http: HttpClient) {
    }

    salvar(idEntregador: bigint, idDeposito: bigint, idCliente: bigint): Observable<Rota> {
        return this.http.post<Rota>(`${RotaService.ENDPOINT_RAIZ_ROTA}?idEntregador=${idEntregador}&idDeposito=${idDeposito}&idCliente=${idCliente}`, null);
    }

    atualizar(id: bigint, idEntregador: bigint, idDeposito: bigint, idCliente: bigint): Observable<Rota> {
        return this.http.put<Rota>(`${RotaService.ENDPOINT_RAIZ_ROTA}/${id}?idEntregador=${idEntregador}&idDeposito=${idDeposito}&idCliente=${idCliente}`, null);
    }

    deletar(id: bigint): Observable<any> {
        return this.http.delete(`${RotaService.ENDPOINT_RAIZ_ROTA}/${id}`);
    }

    buscarRotaPorId(id: bigint): Observable<Rota> {
        return this.http.get<Rota>(`${RotaService.ENDPOINT_RAIZ_ROTA}/${id}`)
    }

    buscarRotaPorCpfCnpj(cpfCnpj: string): Observable<Rota> {
        return this.http.get<Rota>(`${RotaService.ENDPOINT_RAIZ_ROTA}/${cpfCnpj}`)
    }

    getRotas(): Observable<Array<Rota>> {
        return this.http.get<Array<Rota>>(`${RotaService.ENDPOINT_RAIZ_ROTA}`)
    }

}