import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Rota} from "./model/rota.model";
import {RotaService} from "./rota.service";
import {Router} from "@angular/router";
import {FuseAlertType} from "../../../../../@fuse/components/alert";

@Component({
    selector     : 'entregas',
    templateUrl  : './rota.component.html',
    encapsulation: ViewEncapsulation.None
})
export class RotaComponent implements OnInit {
    displayedColumns: string[] = [
        "deposito",
        "cliente",
        "entregador",
        "placaVeiculo",
        "opcoes"
    ];
    rotas: Array<Rota> = [];
    dataSource: MatTableDataSource<Rota>;
    isEmptyList: boolean = false;

    alert: { type: FuseAlertType; message: string } = {
        type: "success",
        message: "",
    };
    showAlert: boolean = false;

    constructor(
        private _rotaService: RotaService,
        private _router: Router
    ){}

    ngOnInit(): void {
        this.carregarRotas();
    }

    carregarRotas() {
        this._rotaService.getRotas().subscribe(
            (response) => {
                this.rotas = response;
                this.isEmptyList = this.rotas.length == 0;
                this.dataSource = new MatTableDataSource<Rota>(
                    this.rotas
                );
            },
            (exception) => {
                console.log(exception);
                this.isEmptyList = true;
            }
        )
    }

    atualizarDataTable() {
        this._rotaService.getRotas().subscribe(
            (response) => {
                this.dataSource.data = response;
                this.isEmptyList = this.dataSource.data.length == 0;
            },
            (exception) => {
                console.log(exception);
                this.isEmptyList = true;
            }
        )
    }

    filtrar(event: Event) {
        const valorFiltro = (event.target as HTMLInputElement).value;
        this.dataSource.filter = valorFiltro.trim().toLowerCase();

    }

    editar(id: string) {
        return `/entregas/rota/cadastro/${id}`;
    }

    deletar(rota: Rota) {
        this._rotaService.deletar(rota.id).subscribe(
            () => {
                this.alert = {
                    type: "success",
                    message: `A rota ${rota.id} foi deletada com sucesso.`,
                };
                this.showAlert = true;
                this.atualizarDataTable();
            },
            (exception) => {
                console.log(exception);
            }
        );
    }
}
