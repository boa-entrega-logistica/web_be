import {Cliente} from "../../../cadastros/cliente/model/cliente.model";

export interface Pedido {
    id?: bigint;
    numNF: bigint;
    cliente: Cliente;
    valor: number;
}