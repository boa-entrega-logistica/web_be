import {Entregador} from "../../../cadastros/entregador/model/entregador.model";
import {Pedido} from "./pedido.model";
import {Deposito} from "../../../cadastros/deposito/model/deposito.model";
import {Cliente} from "../../../cadastros/cliente/model/cliente.model";

export interface Rota {
    id?: bigint;
    pedidos?: Array<Pedido>;
    deposito: Deposito;
    cliente: Cliente;
    entregador: Entregador;
}